# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


report:

一、整體介紹
    1.  最上排的是筆刷選擇，分別是"一般"、"直線"、"長方形"、"三角形"、"圓形"、"寫字"、"橡皮擦"、"圖片"。
    2.  整個畫布由canvas組成。寬、高各500px。
    3.  畫布右邊有兩排工具列，第一排是undo、redo、clear、顏色選擇、筆畫/字體的粗細/大小。
        最下面兩個圖案是選擇實心跟透明，可以套用在畫圖案上面。
    4.  第二排的工具列可以提供User選擇"寫字"時的字型跟文字內容。
        "選擇圖片檔"則是讓User選擇"圖片"，選擇完之後下面會出現圖片的預覽。

|:-------------------------------------------------------------------------------------------------------:|

二、重要的功能介紹
    1.  由於在寫程式的時候，會和html檔進行大量的互動，所以用了一些函式。
        包括"querySelectorAll"、"querySelector"、"getElementById"、"addEventListener"。而有些會寫在html裡面，像是oninput/onchange/onclick = "function_name(input)"等等。
    2.  Canvas相關的funciton，用了最重要的"querySelector"和"getContext('2d')"。前者取得canvas在html檔裡面的ID，後者      則是讓User可以在畫布上做事情。
    3.  在畫畫的時候，無論使用什麼功能，都先分成三個步驟，mousedown、mousemove、mouseup、由addEventListener協助觸發。      進入函式後，再經由switch()來判斷現在是哪種筆刷，最後分別處理。
    4.  一般："一般"的做法很直接，mousedown後筆刷就開始畫，每一點點的移動，筆刷也畫下去，最後mouseup再解除storke()。
    5.  直線、長方形、三角形、圓形：這四個的作法都很類似，都是判斷mousedown的點和mouseup的點。透過這兩個點來畫圖。其中比     較特別的是在滑鼠放開前，圖形會隨著現在滑鼠位置而移動。這個效果是透過快速的清除畫布、重畫畫布產生的效果。
            (1)直線就利用moveTo(起始點)、lineTo(終點)、stroke()三個步驟就能完成。
            (2)長方形就必須算出終點和起始點的水平、垂直距離。最後利用moveTo()、lineTo(四點一個個連起來)就好了，至於要使    用fill()還是stroke()，要看當時情況來決定。
            (3)三角最麻煩，我希望畫出來的是正三角形，起始點為三角形中心點，終點為其中一個頂點。為此，我先假設性的畫出一個    以原點為(0,0)的三角形，其中一個頂點在中心點正右方(x,0)。接著算出現在滑鼠和起始點的相對角度，利用旋轉矩陣     的公式旋轉三個頂點，最後幫三個座標加上(起始點x,起始點y)，就可以取得三角形的三個頂點了。
            (4)圓形必須計算出起始點和終點的距離，以此當作radius，再利用arc()算出圓形的筆跡。
    6.  寫字：必須先在html檔裡面寫兩個東西，一個是選擇字型的<form>，另一個是<input type="text">。
        先用"getElementById"取得那兩個東西。取得後，就可以拿到字型的字串和文字內容的字串。最後分別把字串給.font()和filltext()就可以在畫布上寫出字了。其中在滑鼠放開前可以拖著字移動，利用的前面有寫過的方法做的。
        (題外話，嘗試了stroketext()，做出空心效果，換來一坨大大的雜亂色塊。不知道自己做錯了什麼。)
    7.  橡皮擦：方法和"一般"一模一樣，只是畫筆顏色改成背景色。
    8.  圖片:利用<input type="file">，再利用FileReader()取得image的src。當選取圖片之後，圖片會先出現在下方，提供User      預覽。最後用mousedown、drawImage(src,滑鼠X,滑鼠Y)就可以在畫布上畫出圖案。
        其中預覽效果是先在html建立一個沒有src、有ID的img，再透過選擇完圖片的同時，把圖片的src給那個img。而有時候因為圖片過大，會在css當中給他們max寬高，讓他們可以不要太大。
    9.  顏色選擇:利用<input type = "color">，再透過oninput()取得顏色的字串，最後把字串給fillstyle跟strokestyle。
    10. 字體粗細:利用<input type = "range">，設定max跟min值，再透過oninput()取得字體大小，最後給把值給lineWidth。
    11.下載:下載是用download函式和href函式完成的。
    12.Undo/Redo:在每次離筆之後，利用toDataURL()將圖片記錄下來，需要用到時，用clearRect清空畫布，再用drawImage重畫上                去。
    13.cursor icon:使用內建的cursor，依照現在的畫筆來更換。

三、extra
    1.長方形、三角形、圓形皆分為實心、空心兩種。
    2.滑鼠放開前，圖形會隨著現在滑鼠位置而移動，讓User比較好決定最終形狀。
    3.圖片可先預覽。
