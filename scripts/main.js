
window.onload = function()
{
    
    init();
}
var modeButtons = document.querySelectorAll("a");
var mode = ["normal","line","rectangle","triangle","circle","text","eraser","image"];
var current_mode = "normal";
var current_mode_number = 0;
var cursors = ['copy','crosshair','move','move','move','text','not-allowed','pointer'];
var canvas = document.querySelector("Canvas");
var ctx = canvas.getContext('2d');
var selected_control_room = document.querySelector('h2');
var Font_selection = document.getElementById("Font_selection");
var Text_input = document.getElementById("Text_input");
var color_board = document.querySelector("input[type=color]");
var line_width_control = document.querySelector("input[type=range]");
var line_width_number = document.getElementById("line_width_number");
var RGB = document.getElementById("RGB");
var solid_circle = document.querySelector(".solid-circle");
var hollow_circle = document.querySelector(".hollow-circle");
var pen_style;
var file_Name = document.getElementById("file-name");
var profile_pic = document.getElementById("profile_pic");
var image_Preview = document.getElementById("image-preview");
var pen_color;
var shape = {};
var drag = false;

var cPushArray = new Array();
var cStep = -1;
/*
var rgb = [0,0,0];

var Range = document.querySelectorAll("input[type=range]");
var Text = document.querySelectorAll("input[type=text]");
*/

//var color_demo = document.getElementById("color-demo");
function init()
{
    ctx.fillStyle = "white";
    ctx.fillRect( 0 , 0 , canvas.width , canvas.height );
    
    initButtons();
    set_color('#000000');
    set_width(12);
    set_line_cap();
    cPush();
    
}
function initButtons()
{
    modeButtons[current_mode_number].classList.add('steady');
    canvas.style.cursor = cursors[current_mode_number];
    for (var i = 0; i < modeButtons.length-1; i++) {

        modeButtons[i].addEventListener("click", (function(number) {
            // numCards is 3 in easy mode   
            return function(){
                
                modeButtons[current_mode_number].classList.remove('steady');
                modeButtons[number].classList.add('steady');
                current_mode = mode[number];
                current_mode_number = number;
                //canvas.style.cursor = url(solid.cur);
                canvas.style.cursor = cursors[number];
                //selected_control_room.textContent = 'oh hi mark';

            };
        })(i));
    }
    solid_circle.classList.add('steady');
    pen_style = "solid";
    solid_circle.addEventListener("click" , function(){
        if(pen_style == "hollow")
        {
            solid_circle.classList.add('steady');
            pen_style = "solid";
            hollow_circle.classList.remove('steady');
        }
    });

    hollow_circle.addEventListener("click" , function(){
        if(pen_style == "solid")
        {
            hollow_circle.classList.add('steady');
            pen_style = "hollow";
            solid_circle.classList.remove('steady');
        }
    });

    modeButtons[i].addEventListener('click', function() {
        downloadCanvas(this, 'test.png');
    }, false);

}
function downloadCanvas(link, filename)
{
    link.href = canvas.toDataURL();
    link.download = filename;
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

canvas.addEventListener('mousedown', function(evt) {
    drag = true;
    var mousePos = getMousePos(canvas, evt);
    
    switch(current_mode)
    {
        case "normal" :
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            break;
        case "rectangle":
            shape.startX = mousePos.x;
            shape.startY = mousePos.y;
            break;
        case "line" :
            shape.startX = mousePos.x;
            shape.startY = mousePos.y;
            break;
        case "eraser" :
            //ctx.strokeStyle = canvas.style.backgroundcolor;
            ctx.strokeStyle = canvas.style.backgroundColor;
            //alert(canvas.style.backgroundColor);
            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            evt.preventDefault();
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            break;
        case "circle":
            shape.startX = mousePos.x;
            shape.startY = mousePos.y;
            break;
        case "text" :
            //drag = false;
            //alert(ctx.lineWidth + "px " + Font_selection.value);
            ctx.font =ctx.lineWidth + "px " + Font_selection.value;
            shape.startX = mousePos.x;
            shape.startY = mousePos.y;
            //ctx.fillText(Text_input.value, mousePos.x, mousePos.y); 
            break;
        case "triangle" :
            shape.startX = mousePos.x;
            shape.startY = mousePos.y;
            break;
        case "image" ://
            //alert(image_Preview.width);
            ctx.drawImage(image_Preview, mousePos.x - image_Preview.width/2, mousePos.y - image_Preview.height/2, image_Preview.width, image_Preview.height);
            break;
    }

});

canvas.addEventListener('mousemove', function(evt){
    if(drag)
    {
        var mousePos = getMousePos(canvas, evt);
        
        switch(current_mode)
        {
            case "normal" :
            
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                break;
            case "rectangle" :
            case "circle" :
            case "line" :
            case "triangle" :
            case "text" :
            //case "image" :

                var canvasPic = new Image();
                canvasPic.src = cPushArray[cStep];
                canvasPic.onload = function ()
                { 
                    //ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                    //ctx.clearRect(shape.startX,shape.startY,shape.startX+shape.w,shape.startY + shape.h);
                    //ctx.clearRect(shape.startX,shape.startY,shape.w,shape.h);
                    ctx.clearRect(0,0,canvas.width,canvas.height);
                    ctx.drawImage(canvasPic, 0, 0); 
                    shape.w = mousePos.x - shape.startX;
                    shape.h = mousePos.y - shape.startY;
                    draw();
                }
                break;

            case "eraser" :
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                break;
            
        }

    }
    



}, false);

canvas.addEventListener('mouseup', function(evt) 
{
    if(drag)
    {
        
        var mousePos = getMousePos(canvas, evt);
        switch(current_mode)
        {
            case "normal" :
                break;
            case "eraser" :
                ctx.closePath();
                ctx.strokeStyle = pen_color;
                break;
            case "rectangle" :
            case "line" :
            case "circle" :
            case "triangle" :
                shape.w = mousePos.x - shape.startX;
                shape.h = mousePos.y - shape.startY ;
                draw();
                break;
            
            
        }

       drag = false;
        cPush();    
            
            
        //
        
    }
    
    

    //canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

canvas.addEventListener('mouseleave', function() 
{
    if(drag)
    {
        if(current_mode == "normal" || current_mode == "eraser")
        {
            drag = false;
            cPush();
        }
        
        //drag = false;
        
    }
    

    //canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

function draw() 
{
    
    switch(current_mode)
    {
        case "rectangle" :
            //ctx.fillRect(shape.startX, shape.startY, shape.w, shape.h);
            ctx.beginPath();
            ctx.moveTo(shape.startX + shape.w , shape.startY + shape.h);
            ctx.lineTo(shape.startX           , shape.startY + shape.h);
            ctx.lineTo(shape.startX           , shape.startY          );
            ctx.lineTo(shape.startX + shape.w , shape.startY          );
            ctx.lineTo(shape.startX + shape.w , shape.startY + shape.h);

            if(pen_style == "solid")ctx.fill();
            else                    ctx.stroke();
            
            ctx.closePath();
            break;
        case "circle" :
            ctx.beginPath();
            ctx.arc(shape.startX, shape.startY, Math.pow(shape.w*shape.w + shape.h*shape.h,0.5), 0, Math.PI*2);
            
            if(pen_style == "solid")ctx.fill();
            else                    ctx.stroke();
            
            ctx.closePath();
            break;
        case "triangle" :
            //shape.r = Math.pow(shape.w*shape.w + shape.h*shape.h,0.5);
            //cos = shape.w/shape.distance
            //sin = shape.h/shape.distance
            ctx.beginPath();
            ctx.moveTo(shape.startX + shape.w , shape.startY + shape.h);
            ctx.lineTo(shape.startX - 1/2*shape.w - Math.pow(3,0.5)*shape.h/2 , shape.startY - 1/2*shape.h + Math.pow(3,0.5)*shape.w/2);
            ctx.lineTo(shape.startX - 1/2*shape.w + Math.pow(3,0.5)*shape.h/2 , shape.startY - 1/2*shape.h - Math.pow(3,0.5)*shape.w/2);
            ctx.lineTo(shape.startX + shape.w , shape.startY + shape.h);
            
            if(pen_style == "solid")ctx.fill();
            else                    ctx.stroke();
            
            ctx.closePath();
            break;
        case "line" :
            ctx.beginPath();
            ctx.moveTo(shape.startX , shape.startY);
            ctx.lineTo(shape.startX + shape.w , shape.startY + shape.h);
            
            ctx.stroke();
            
            ctx.closePath();
            break;
        case "text" :
            ctx.fillText(Text_input.value , shape.startX + shape.w , shape.startY + shape.h); 
            
            //ctx.strokeText(Text_input.value , shape.startX + shape.w , shape.startY + shape.h); 
            
    }

    
}

document.getElementById('reset').addEventListener('click', function()
{
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

function change_color()
{
    //alert(color_board.value[0]);
    set_color(color_board.value);
}

function set_color(str)
{
    pen_color = str;
    ctx.strokeStyle = str;
    ctx.fillStyle=str;

    //RGB.textContent = "(r,g,b) = ("0,0,0)
    /*
    ctx.strokeStyle = "rgb(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")";
    color_demo.style.backgroundColor = "rgb(" + rgb[0] + ", " + rgb[1] + ", " + rgb[2] + ")";
    */
}

function change_width()
{
    //alert(color_board.value[0]);
    set_width(line_width_control.value);
}

function set_width(wid)
{
    ctx.lineWidth = wid;
    line_width_control.value = wid;
    line_width_number.textContent = wid;
}

function set_line_cap()
{
    ctx.lineCap = "round";
}

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(profile_pic.files[0]);

    oFReader.onload = function (oFREvent) {
        image_Preview.src = oFREvent.target.result;
    };
    //alert(image_Preview.src);
}

// ctx = document.getElementById('myCanvas').getContext("2d");
	
function cPush() {
    
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    
    //alert(canvas.toDataURL());
    cPushArray.push(canvas.toDataURL());
    //alert("sdf");
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, 600, 600);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            ctx.clearRect(0, 0, 600, 600);
            ctx.drawImage(canvasPic, 0, 0); 
        }
    }
}

function clean_the_canvas()
{
    ctx.fillStyle = "white";
    ctx.fillRect( 0 , 0 , canvas.width , canvas.height );
    ctx.fillStyle = ctx.strokeStyle;
    cPush();
}